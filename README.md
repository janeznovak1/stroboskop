# Spletni barvni stroboskop

1\. domača naloga pri predmetu [Osnove informacijskih sistemov](https://ucilnica.fri.uni-lj.si/course/view.php?id=54) (navodila)


## 6.1 Opis naloge in navodila

Na Bitbucket je na voljo javni repozitorij [https://bitbucket.org/asistent/stroboskop](https://bitbucket.org/asistent/stroboskop), ki vsebuje nedelujoč spletni stroboskop. V okviru domače naloge ustvarite kopijo repozitorija ter popravite in dopolnite obstoječo implementacijo spletne strani tako, da bo končna aplikacija z vsemi delujočimi funkcionalnostimi izgledala kot na spodnji sliki. Natančna navodila si preberite na [spletni učilnici](https://ucilnica.fri.uni-lj.si/mod/workshop/view.php?id=19181).

![Stroboskop](stroboskop.gif)

## 6.2 Vzpostavitev repozitorija

Naloga 6.2.2:

git clone https://janeznovak1@bitbucket.org/janeznovak1/stroboskop.git

Naloga 6.2.3:

https://bitbucket.org/janeznovak1/stroboskop/commits/8939c44ae42ab504a2caaf21aa09a44df7763320?at=master

## 6.3 Skladnja strani in stilska preobrazba

Naloga 6.3.1:

https://bitbucket.org/janeznovak1/stroboskop/commits/dfebc7c7b03d65182eec8cf4d092f75aa2dc7f8b?at=master

Naloga 6.3.2:

https://bitbucket.org/janeznovak1/stroboskop/commits/df25b64797c3f94f7206649f9caba9ee427d7773?at=master

Naloga 6.3.3:

https://bitbucket.org/janeznovak1/stroboskop/commits/c4f454da0928af61b7f8fe56c40586c1e5aa826f?at=master

Naloga 6.3.4:

https://bitbucket.org/janeznovak1/stroboskop/commits/1c27f69f036fca33b7211e8d2448cc891bd90bee?at=master

Naloga 6.3.5:

git checkout master

git pull origin master

git merge izgled

git push origin master

## 6.4 Dinamika in animacija na strani

Naloga 6.4.1:

https://bitbucket.org/janeznovak1/stroboskop/commits/a477666f728076f90ae6aa197612a75bdbd0b807?at=master
in
https://bitbucket.org/janeznovak1/stroboskop/commits/79ed6c478b636310b960cb33dcf8018c4a8d7cbd?at=master

Naloga 6.4.2:

https://bitbucket.org/janeznovak1/stroboskop/commits/ef2c1b0a54c7eff828715f51d445b81c84a3227c?at=master

Naloga 6.4.3:

https://bitbucket.org/janeznovak1/stroboskop/commits/bd4717aece8bd28d9d3df3087e6d1a5600389f91?at=master

Naloga 6.4.4:

https://bitbucket.org/janeznovak1/stroboskop/commits/0b146365584fe334aae9241230a50c5c20c95eda

Naloga 6.4.5

git checkout master

git pull origin master

git merge dinamika

git push origin master

Urejanje README.md

git commit -a -m "Urejanje README.md"